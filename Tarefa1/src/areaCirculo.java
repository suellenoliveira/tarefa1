import java.util.Scanner;

public class areaCirculo {
	
	public static void main(String[] args) {
		Scanner tecla = new Scanner(System.in);
		final double pi = 3.14;
		double raio, area;
		
		System.out.println("Raio: ");
		raio = tecla.nextDouble();
		
		area = pi*(raio*raio);
		
		System.out.println("Area: " + area);

	}

}
